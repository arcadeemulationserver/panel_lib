panel_lib = {}
local version = "2.1.0"

dofile(minetest.get_modpath("panel_lib") .. "/api.lua")

minetest.log("action", "[PANEL_LIB] Mod initialised, running version " .. version)
